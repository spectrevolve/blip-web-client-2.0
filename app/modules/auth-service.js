angular.module('AuthService', [])

  .factory('AuthService', function() {
    var currentUser;
    var currentID;
    var currentFollowList;
    
    return {
      
      login: function(user,id) {
        currentUser = user;
        currentID = id;
      },
      
      logout: function() {
        currentUser = '';
        currentID = '';
      },
      
      currentUser: function() {
        return currentUser ? currentUser : localStorage.getItem('user');
      },
      
      currentID: function() {
        return currentID ? currentID : localStorage.getItem('id');
      },
      
      currentFollow: function() {
        return currentFollowList ? currentFollowList : localStorage.getItem('followers');
      }
      
    }
});