// Filter to only get blips of users currently on followerList

angular.module('BlipFilter', [])

  .filter('blipSearch', function() {
    return function(input, search, followers) {
      
      if (!input) return input;
      if (!search) return search;
      
      var result = {};
      
      angular.forEach(input, function(value,key) {
        
        if((search === value.owner || search === value.tag) && value.owner in followers) {
          console.log("Match");
          result[key] = value.comment;
        }
        
      });
      
      return result;
    }
});
  
