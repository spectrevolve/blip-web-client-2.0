/*
  AngularJS Code Sample:
  
  This code sample features a sample login controller for the Blip Web Client
    // Login Function 
    // 1. Check if username exists
    // 2. If exists: Check if password matches
    // 3. If matches: Call Signin Function
    // * Any errors: Throw error to user
*/

angular.module('formController', ['AuthService'])
  
  .controller('FormCtrl', function($scope,$location,AuthService) {
    
    
    $scope.handleLoginForm = function(user) {
      REF.child('clients').once('value').then(function(snapshot) {
        if(snapshot.hasChild(user.username)) {
          REF.child('clients').child(user.username).once('value').then(function(snapshot) {
            if(snapshot.val().password === user.password) {
              AUTH.signInWithEmailAndPassword(snapshot.val().email,snapshot.val().password).then(function(authData) {
                AuthService.login(user.username,authData.uid);
                $isLoginFailure = false;
                localStorage.setItem('user', user.username);
                localStorage.setItem('id', authData.uid);
                console.log(snapshot.val().following);
                localStorage.setItem('followers', JSON.stringify(snapshot.val().following));
                $location.url('/world');
                $scope.$apply();
              }).catch(function(err) {
                $scope.isLoginFailure = true;
                $scope.errorLoginMessage = err.message;
                $scope.$apply();
              });
            } else {
              $scope.isLoginFailure = true;
              $scope.errorLoginMessage = 'Username or password is incorrect';
              $scope.$apply();
            }
          }).catch(function(err) {
            $scope.isLoginFailure = true;
            $scope.errorLoginMessage = err.message;
            $scope.$apply();
          });
        } else {
          $scope.isLoginFailure = true;
          $scope.errorLoginMessage = 'Username or password is incorrect';
          $scope.$apply();
        }
      }).catch(function(err) {
        $scope.isLoginFailure = true;
        $scope.errorLoginMessage = err.message;
        $scope.$apply();
      });
    }
    
    // Register Function
    // 1. Check if username is taken
    // 2. If not taken: Call Createuser Function
    // * Any errors: Throw error to user
    $scope.handleRegisterForm = function(user) {
      REF.child('clients').once('value').then(function(snapshot) {
        if(snapshot.hasChild(user.username)) {
          $scope.isModalInvalid = true;
          $scope.errorModalMessage = "Username is already taken.";
          $scope.$apply();
        } else {
          AUTH.createUserWithEmailAndPassword(user.email,user.password).then(function(authData) {
            REF.child('clients').child(user.username).set({
              'email': user.email,
              'password': user.password,
              'following': ''
            });
            
            // Update Profile
            var ussr = firebase.auth().currentUser;
            
            ussr.updateProfile({
              displayName: user.username,
              photoURL: 'http://www.porticodesign.com/wp-content/uploads/2014/03/blank-person-07d1653f840307220b203ecb834f5904-400x400.png'
            }).then(function () {
              console.log('Update Success');
              
              $scope.isModalInvalid = false;
              $scope.successModalMessage = "Successfully Registered!";
              $scope.$apply();
            }, function(error) {
              console.log(error);
            });
  
          }).catch(function(err) {
            $scope.isModalInvalid = true;
            $scope.errorModalMessage = err.message;
            $scope.$apply();
          });
        }
      });
    }   
});
  




