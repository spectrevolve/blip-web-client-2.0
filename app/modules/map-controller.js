angular.module('mapController', ['AuthService','ngGeolocation', 'BlipFilter'])

  .controller('MapCtrl', function($scope,$location,$timeout,$compile,$geolocation,AuthService) {
    
    // Instantiate variables for Map & Blips
    $scope.map;
    $scope.blips = {};
    $scope.blipFilter = [];
    
    // Current User | Current User ID | Current Location
    $scope.currentUser = AuthService.currentUser();
    $scope.currentID = AuthService.currentID();
    var followDispatch = AuthService.currentFollow();
    if(followDispatch) {
      $scope.currentFollowList = JSON.parse(followDispatch);
      
      firebase.database().ref('/clients/' + $scope.currentUser + '/following').once('value').then(function(snapshot) {
        $scope.currentFollowList = snapshot.val();
        console.log($scope.currentFollowList); 
      })
    }
    
    
    // Redirect to Login Screen if not logged in
    if(!$scope.currentUser) {
      $location.url('/');
    }
    
    // Get Blips From Firebase
    $scope.ref = REF;
    $scope.ref.child('blips').once('value').then(function(snapshot) {
      snapshot.forEach(function(childsnapshot) {
        $scope.blips[childsnapshot.val().ID] = childsnapshot.val();
      });
      
      // Get Geolocation coordinates
      $geolocation.getCurrentPosition({
        timeout: 500
      }).then(function(position) {
        $scope.coords = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
      
        // Create Map Here
        $scope.createMap();
      }); 
    });
    
    // Create Google Map | Objects | Listeners
    $scope.createMap = function() {
      
      // Google Map
      $scope.map = new google.maps.Map(document.getElementById('map'),
      {
        zoom: 12,
        center: $scope.coords
      });
      
      // Current Location Marker
      $scope.centralMark = new google.maps.Marker({
        position: $scope.coords,
        map: $scope.map,
        icon: 'app/img/gm.png',
        title: 'CENTRAL'
      });
      
      // Circle Property: Calculate Blips Inside Circle
      google.maps.Circle.prototype.contains = function(latLng) {
        return this.getBounds().contains(latLng) && google.maps.geometry.spherical.computeDistanceBetween(this.getCenter(), latLng) <= this.getRadius();
      }
      
      // Circle 
      $scope.circle = new google.maps.Circle({
        strokeColor: '#001532',
        strokeWeight: 2,
        fillColor: '#69BE28',
        map: $scope.map,
        center: $scope.coords,
        radius: 3000,
        draggable: true
      });
      
      // Circle Listener: Reveal Markers inside circle after dragging
      $scope.circle.addListener('dragend', function() {
        
        for(i in $scope.blips) {
          $scope.circle.contains($scope.blips[i].marker.position) ? $scope.blips[i].marker.setMap($scope.map) : $scope.blips[i].marker.setMap(null);
        }
      });
      
      // Circle Listener: Right Click inside circle when right clicking
      $scope.circle.addListener('rightclick', function(e) {
        $('#createModal').modal('show');
        $scope.tempLat = e.latLng.lat();
        $scope.tempLng = e.latLng.lng();
      });
      
      // Create Markers for Blips From Firebase
      $scope.setupDBMarkers();
    }
    
    // Function to add blips as Markers to Map
    $scope.setupDBMarkers = function() {
      for(i in $scope.blips) {
       
        // InfoWindow Template For Each Blip Marker
        var comment = genIW($scope.blips[i].ID,$scope.blips[i].comment, $scope.blips[i].owner, $scope.blips[i].tag, $scope.blips[i].url);
        
        // Compile comment for access to angular js 
        var compiled = $compile(comment)($scope);
        
        // Create Marker
        var marker = new google.maps.Marker({
          position: { lat: parseFloat($scope.blips[i].x), lng: parseFloat($scope.blips[i].y) },
          icon: 'app/img/blip-icon-sm.png',
          map: $scope.map,
          likes: $scope.blips[i].likes,
          dislikes: $scope.blips[i].dislikes,
          replies: $scope.blips[i].replies
        });
        
        // Attach marker to appropriate set
        $scope.blips[i].marker = marker;
        
        // Attach compiled InfoWindow to newly created marker
        $scope.attachInfoWindow($scope.blips[i].marker, compiled[0]);
      }
    }
    
    // Function that attaches InfoWindow to a specific Marker & template
    $scope.attachInfoWindow = function (mark, msg) {
      var infowindow = new google.maps.InfoWindow({
        content:  msg,
        maxWidth: 1000,
        maxHeight: 1000
      });
        
      mark.addListener('click', function() {
        infowindow.open(mark.get('map'), this);
      });  
   }
   
   // TODO: Complete Like Function => Other Users
   $scope.addLike = function(ID) {
     
     $scope.blips[ID].likes++;
     
     firebase.database().ref('blips/' + $scope.blips[ID].ID).update({
       likes: $scope.blips[ID].likes
     });
     
     // Add event handler => In main area (maybe add the handlers at the bottom of this file -- or not)
   }
   
   // TODO: Complete Dislike Function => Other Users
   $scope.addDislike = function(ID) {
     
     $scope.blips[ID].dislikes++;
     
     firebase.database().ref('blips/' + $scope.blips[ID].ID).update({
       dislikes: $scope.blips[ID].dislikes
     });
     
     // Add event handler => Same as the above one
   }
   
   // TODO: Complete Reply System => To DB || Other Users
   $scope.addReply = function(ID) {
     if(!$scope.blips[ID].replies) {
       $scope.blips[ID].replies = [];
     }
     $scope.blips[ID].replies.push({ 
       reply: $scope.recon, user: $scope.currentUser
     });
     
     var position = $scope.blips[ID].replies.length-1;
     
     firebase.database().ref('blips/' + $scope.blips[ID].ID + '/replies/' + position).set({
       reply: $scope.recon,
       user: $scope.currentUser
     });
     
     $scope.recon = '';
     
     // Add event handler => Same as above
   }
   
   // TODO: Complete Blip Create Function
   $scope.createBlip = function(newcom,newtag) {
     
     //ID
     var time = Date.now();
     var date = new Date();
     $scope.formatDate(date);
     var dateString = $scope.formatDate(date);
     
     // Create Object to push to markers
     var newBlip = {
       ID: time + $scope.currentUser,
       comment: newcom,
       dislikes: 0,
       likes: 0,
       owner: $scope.currentUser,
       replies: '',
       tag: newtag,
       timestamp: dateString,
       url: localStorage.getItem('pic_url'),
       x: $scope.tempLat,
       y: $scope.tempLng
     }
     
     // Store Blip to DB and marker array
     $scope.blips[newBlip.ID] = newBlip;
     
     firebase.database().ref('blips/' + newBlip.ID).set({
       ID: newBlip.ID,
       comment: newcom,
       dislikes: 0,
       likes: 0,
       owner: newBlip.owner,
       replies: "",
       tag: newBlip.tag,
       timestamp: dateString,
       url: newBlip.url,
       x: newBlip.x,
       y: newBlip.y
     }).then(function () {
       
      // Create new marker
      var new_comment = genIW(newBlip.ID,newcom,$scope.currentUser,newBlip.tag,newBlip.url);
      var new_comp = $compile(new_comment)($scope);
       
      var new_marker = new google.maps.Marker({
        position: { lat: parseFloat(newBlip.x), lng: parseFloat(newBlip.y) },
        icon: 'app/img/blip-icon-sm.png',
        map: $scope.map,
        likes: $scope.blips[newBlip.ID].likes,
        dislikes: $scope.blips[newBlip.ID].dislikes,
        replies: $scope.blips[newBlip.ID].replies
      });
         
      $scope.blips[newBlip.ID].marker = new_marker;
       
      $scope.attachInfoWindow($scope.blips[newBlip.ID].marker, new_comp[0]);
     });
   }
   
   // Function to format date
   $scope.formatDate = function(dt) {
     var dateFormat = dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds() + ' ' + (dt.getMonth()+1) + '/' + dt.getDate();
     
     return dateFormat;
   }
   
   // Function Add Follower To User List
   $scope.addFollower = function(usr) {
     
     console.log(usr);
     
     if($scope.currentFollowList === "") {
       
       $timeout(function() {
         $scope.$apply(function() {
           $scope.currentFollowList = {};
           $scope.currentFollowList[usr] = '';
           firebase.database().ref('/clients/' + $scope.currentUser + '/following/' + usr).set('');
           $scope.follower = '';
         })
       }, 100);
       
       return usr;
     }
     
     if(usr in $scope.currentFollowList) {
       console.log("already in list");
     } else {
       for(blip in $scope.blips) {
         if($scope.blips[blip].owner === usr) {
           $scope.currentFollowList[usr] = '';
           
           firebase.database().ref('/clients/' + $scope.currentUser + '/following/' + usr).set('');
           break;
         }
       }
     }
     $scope.follower = '';
   }
   
   // Event Handlers To Receive Data
   // Like/Dislike & Replies
   firebase.database().ref('/blips').off('child_changed');
   
   firebase.database().ref('/blips').on('child_changed', function(childSnapshot, prevChildKey) {
     $timeout(function() {
       $scope.$apply(function() {
         $scope.blips[childSnapshot.val().ID].likes = childSnapshot.val().likes;
         $scope.blips[childSnapshot.val().ID].dislikes = childSnapshot.val().dislikes;
         $scope.blips[childSnapshot.val().ID].replies = childSnapshot.val().replies;
       });
     }, 100);
   });
  
   // Function to Logout Current User   
   $scope.handleLogout = function() {
      AuthService.logout();
      localStorage.removeItem('user');
      localStorage.removeItem('id');
      localStorage.removeItem('followers');
      $location.url('/');
    }
});