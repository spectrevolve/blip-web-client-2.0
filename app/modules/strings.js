// Will generate appropriate info window for follower / non-follower

function genIW (id, com, own, tag, imgpath) {
  
   var blip_comment = 
    `<div style="width:300px; overflow-y:hidden;">
       <img src='${imgpath}' class="img-thumbnail" width="100%;">
       <span style="margin-top: 100px;">
         <h4 style="display: inline;"><strong>${own}</strong></h4>
         <span class="pull-right">
           <span style="font-size:1.3em; color:blue; cursor:pointer;" class="glyphicon glyphicon-thumbs-up" ng-click="addLike('${id}')">{{blips['${id}'].likes}}</span>
           <span style="font-size:1.3em; color:red; cursor:pointer;" class="glyphicon glyphicon-thumbs-down" ng-click="addDislike('${id}')">{{blips['${id}'].dislikes}}</span>
         </span>
       </span>
       <h4>${com}</h4>
       <h5><strong>Tag: ${tag}</strong></h5>
       <form class="form-group" ng-submit="addReply('${id}')">
         <input required ng-model="recon" style="width:100%;" class="form-control blip-btn" placeholder="Leave a Reply" value=recon />
       </form>
       <h4><strong>Replies: {{blips['${id}'].replies.length}}</strong></h4>
       <div style="word-wrap: break-word;" ng-repeat="reply in blips['${id}'].replies track by $index">{{reply.reply}} - <strong>{{reply.user}}</strong></div>
     </div>`;
   
   return blip_comment;
}