// Firebase Config
var config = {
        apiKey: 'AIzaSyAXnjQDvPAhNICBGySZcEffdslzs8ND6FI',
        authDomain: 'blipster.firebaseapp.com',
        databaseURL: 'https://blipster.firebaseio.com/',
        storageBucket: 'project-9136517748583089120.appspot.com'
      }

// Firebase Instantiation
firebase.initializeApp(config);
var REF = firebase.database().ref();
var AUTH = firebase.auth();
var STOR = firebase.storage().ref();

// Blip Angular Root --> Routes
var Blip = angular.module('Blip', [
  'ngRoute',
  'formController',
  'mapController'
]);

Blip.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'app/views/login.html',
        controller: 'FormCtrl'
      }).
      when('/world', {
        templateUrl: 'app/views/map.html',
        controller: 'MapCtrl'
      }).
      otherwise({
        redirectTo: '/'
      });
}]);