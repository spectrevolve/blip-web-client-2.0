// Basic Server: Two Main Tasks

// 1. Deliver the client application
var express = require('express');
var app = express();
var firebase = require('firebase');

var REF = new Firebase("https://blipster.firebaseio.com/blips");

app.use(express.static(__dirname));

app.get('/', function(req,res) {
  res.sendFile(__dirname+'/app/index.html');
});

app.listen(process.env.PORT || 3000, function() {
  console.log('Example app listening on port 3000!');
});

// 2. Set timer for Blip deletion (48 hours)

var current_time = Date.now();
var timespan = current_time - 48 * 60 * 60 * 1000;
var current_Date = new Date();

REF.on('value', function(snapshot) {
  snapshot.forEach(function(childsnapshot) {
    var test = new Date(childsnapshot.val().timestamp);
    if( test < current_Date) {
      console.log(test + ' || ' + current_Date);
      // Delete the following blips -- Demo
    }
    
  })
});